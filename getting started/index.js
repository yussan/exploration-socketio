const app = require("express")();
const cookieParser = require("cookie-parser");
const session = require("express-session");
const http = require("http").Server(app);
const io = require("socket.io")(http);

// set session
app.use(cookieParser());
app.use(session({secret: 'getting-started-socketio'}));
app.use(function(req, res, next) {
  console.log("your session id is", req.session.id);
  return next()
})

// app routes
app.get("/", function(req, res) {
  res.sendFile(__dirname + "/index.html");
});


// TODO : make socket can read req.session
// socket config
io.on("connection", function(socket) {
  console.log("a user connected.");
  
  socket.on("chat message", function(msg) {
    console.log("new message: \"" + msg + "\"");
    console.log("session", socket.request)
    io.emit('chat message', msg)
  });
  socket.on("disconnect", function() {
    console.log("a user disconnected.");
  });
});

http.listen(3000, function() {
  console.log("listening on port 3000");
});
